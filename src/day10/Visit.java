package day10;


	import java.io.IOException;
	import java.nio.file.Files;
	import java.nio.file.Path;
	import java.nio.file.Paths;
	import java.util.ArrayList;
	import java.util.Collections;
	import java.util.Comparator;
	import java.util.List;

	public class Visit {

		String kuupaev;
		int kulalisteArv;
		
		public String getKuupaev() {
			return kuupaev;
		}

		public void setKuupaev(String kuupaev) {
			this.kuupaev = kuupaev;
		}

		public int getKulalisteArv() {
			return kulalisteArv;
		}

		public void setKulalisteArv(int kulalisteArv) {
			this.kulalisteArv = kulalisteArv;
		}

		
		public static List <Visit> kulalisteArv2 = new ArrayList<>(); //siia tulevad objektid Visit klassist
		
		public Visit(String kuupaev, int kulalisteArv) {
			this.kuupaev = kuupaev;
			this.kulalisteArv = kulalisteArv;
		}

		
		public void kulalisedRitta(String filePath) throws IOException{
			Path path = Paths.get(filePath);
			List<String> fileLines = Files.readAllLines(path);
			for (String fileLine : fileLines) {
				String[] lineParts = fileLine.split(", ");
				kulalisteArv2.add(new Visit(lineParts[0], Integer.parseInt(lineParts[1])));
			}
		}
		
//		public int compareTo(int data1, int data2) {
//			
//			return data1 > data2 ? 1 : data1 < data2 ? -1 : 0;
//						
//		}
		
		public static void jarjendikontroll() {
			Collections.sort(kulalisteArv2, new Comparator<Visit>() {
				public int compare(Visit o1, Visit o2) {
					return Integer.compare(o2.kulalisteArv, o1.kulalisteArv);
				}
			});
			
			System.out.println("Sorted Array List");
			System.out.println(kulalisteArv2.toString());}
			
//			for (int i=0; i<arvud.length; i++) {
//				compareTo(arvud[i], arvud[i+1]);
//			}
			
		
		@Override
		public String toString() {
			return String.format("[%s, %s]", this.getKulalisteArv(), this.getKuupaev());
		}
	
		
	}