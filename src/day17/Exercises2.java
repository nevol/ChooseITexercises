package day17;

public class Exercises2 {

	public static void xxx(String list, String list2) {

	}

	public static void yyy(String[] text) {

	}

	public static boolean www (int[] a, String[] b) {
		return true;
	}

	// 1
	public static int number() {
		return 0;
	}

	// 2
	public static int sum(int a, int b) {
		return a + b;
	}

	public static void main(String args[]) {

		int a = number();
		System.out.println(a);
		System.out.println(sum(3, 4));
		System.out.println(pricetWithVat(100)); //�l3
		System.out.println(abc(true, false));
		System.out.println(netoPind(3, 4));
		System.out.println(korteriPindala(6, 3, 3, 5));

		int[][] maja1Ruumid = { 
				{ 3, 5 }, // toa 1 m��dud
				{ 4, 5 }, // toa 2 m��dud
				{ 2, 3 }, };
		int[][] maja2Ruumid = { { 6, 2 }, { 4, 2 }, { 2, 2 }, };

		int maja1Pindala = majaPindalaKokku(maja1Ruumid);
		int maja2Pindala = majaPindalaKokku(maja2Ruumid);
		System.out.println("Maja suletud netopind on " + maja1Pindala + " m2.");
		System.out.println("Maja suletud netopind on " + maja2Pindala + " m2.");

		int[] myNymbers = { 3, 6, 8, 9, 25, 17 };
		int myMaxNumber = getMaxNumber(myNymbers);
		System.out.println(myMaxNumber);

		int[] mySumma = { 3, 5, 6 };
		float mySummaKokku = numbriteSummaKokku(mySumma);
		System.out.println(mySummaKokku);
	}

	// �l 3
	public static float pricetWithVat(float withoutVat) {
		return withoutVat * 1.2f;
	}

	// 4
	public static int abc(boolean i, boolean j) {
		if (i == j) {
			return 1;
		} else {
			return 0;
		}
	}

	public static int netoPind(int pikkus, int laius) {
		return pikkus * laius;

	}

	public static int korteriPindala(int pikkus1, int laius1, int pikkus2, int laius2) {
		int toa1Pindala = netoPind(pikkus1, laius1);// meetodis kutsume meetodi v�lja
		int toa2Pindala = netoPind(pikkus2, laius2);
		return toa1Pindala + toa2Pindala;
	}

	public static int majaPindalaKokku(int[][] ruumideM��dud) {
		int pindalaKokku = 0;
		for (int[] tuba : ruumideM��dud) {
			pindalaKokku = pindalaKokku + netoPind(tuba[0], tuba[1]);
		}
		return pindalaKokku;
	}

	public static int getMaxNumber(int[] numbers) {
		int maxNumber = 0;
		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] > maxNumber) {
				maxNumber = numbers[i];
			}
		}
		return maxNumber;
	}

	public static float numbriteSummaKokku(int[] arvud) {
		float summaKokku = 0;
		for (int i = 0; i < arvud.length; i++) {
			summaKokku = summaKokku + arvud[i];

		}
		return summaKokku / arvud.length;
	}
}
