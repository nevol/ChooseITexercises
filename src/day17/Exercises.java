package day17;

public class Exercises {

	public static void main(String[] args) {
		float priceWithoutVat1 = getPriceWithoutVat(120.00f); //�l 1 v�ljakutsumine kahe tehtena
		System.out.println(priceWithoutVat1); 
		System.out.println(getPriceWithoutVat(120f));//�l 1 �he tehtena
		
		
		System.out.println(isPalindrome("Aias sajas saia"));
	}
		
	//�l 1	
	public static float getPriceWithoutVat(float priceWithVat) {
			return priceWithVat / 1.2f;
	}
	
	//�l 2
	public static boolean isPalindrome (String text) {
		//String text2 = new StringBuilder(text).reverse().toString(); �l.2 1 variant
		String text2 = "";
		for(char c : text.toCharArray()) {
			text2 = c + text2;
		}
		return text.equalsIgnoreCase(text2);
		
	}
	
	
	
}
