package day05;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Exercises2 {

	public static void main(String[] args) {
		
//		//�lesanne 5
//		Scanner inputScanner = new Scanner(System.in);
//		String osad = inputScanner.nextLine();
//		String[] rates = osad.split(" ");
//		
//		int rate = Integer.parseInt(rates[1]);
//		if (rate <51) {
//			System.out.println("Fail");
//			
//		}else if (50<rate && rate<=60) {
//			System.out.println("Pass - hinne 1, punktide summa " + rate);
//		}else if (60<rate && rate<=70) {
//				System.out.println("Pass - hinne 2, punktide summa " + rate);
//		}else if (70<rate && rate<=80) {
//			System.out.println("Pass - hinne 3, punktide summa " + rate);
//		}else if (80<rate && rate<=90) {
//			System.out.println("Pass - hinne 4, punktide summa " + rate);
//		}else if (90<rate && rate<=100) {
//			System.out.println("Pass - hinne 5, punktide summa " + rate);
//
//	}

	//�l 7 
		
		Map<String, String[]> countryLanguage = new HashMap<>();
		countryLanguage.put("Estonia", new String[] { "Estonian", "Russian", "Finnish", "Ukrainian", "Belarusian"});
		countryLanguage.put("Latvia", new String[] { "Latvian", "Russian", "Poland", "Ukrainian" });
		

		// Printout: Variant 1
		for (String key : countryLanguage.keySet()) {
			System.out.println("Country: " + key);
			System.out.println("Language:");
			for (String city : countryLanguage.get(key)) {
				System.out.println("    " + city);
			}
		}

		// Printout: Variant 2
//		countryLanguage.forEach((countryName, cityNames) -> {
//			System.out.println("Country: " + countryName + "\nLanguage:" );
//			for(String cityName : cityNames) {
//				System.out.println("    " + cityName);
//			}
//		});
}
}
