package day16;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Country {
	
	private int population = 0;
	private String name = null; 
	public List<String> languages = new ArrayList<>();
	

	public int getPopulation() {
		return population;
	}
	public void setPopulation(int population) {
		this.population = population;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getLanguages() {
		return languages;
	}
	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}
		
	public Country (String name, String population, Collection<String> languages) {
		
	}

	@Override
	public String toString() {
		
		return null;
	}
}

