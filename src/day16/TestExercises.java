package day16;

import java.util.Arrays;
import java.util.Collection;

public class TestExercises {

	public static void main(String[] args) {

		System.out.println(kasV�rdne(8, 9));// �l 2 v�ljakutsumine

		
		String[] number = { "�ks", "viisteist", "kaks" }; // �l 3 v�ljakutsumine
		for (int arv : myNumbers(number)) {
			System.out.println("S�na sisaldab " + arv + " t�hti.");
		}
		
	
	//�l 1
	//Deklareeri muutuja, mis hoiaks endas Pi (https://en.wikipedia.org/wiki/Pi) arvulist v��rtust v�hemalt 11 kohta peale koma. 
	//Korruta selle muutuja v��rtus kahega ja prindi standardv�ljundisse.
		double pi = Math.PI;
		System.out.printf("%.11f\n", pi*2);
		
	//�l4
	System.out.println(centuryNumber(1876));//�l 4 v�ljakutsumine
		
	// �l 5

//		 Country myCountry = new Country();
//		 myCountry.setName("Estonia");
//		 myCountry.setPopulation(1300000);
//		 myCountry.languages.addAll(Arrays.asList("Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"));
//		 
	}
	
	
	

	// �l 2
	public static boolean kasV�rdne(int a, int b) {
		if (a == b) {
			System.out.println("Etteantud arvud " + a + " ja " + b + " on v�rdsed.");
			return true;
		} else {
			System.out.println("Etteantud arvud " + a + " ja " + b + " ei ole v�rdsed.");
			return false;
		}
	}



	
	// �l 3 Kirjuta meetod, mille sisendparameetriks on Stringide massiiv ja mis
	// tagastab t�isarvude massiivi.
	// Tagastatava massiivi iga element sisaldab endas vastava sisendparameetrina
	// vastu v�etud massiivi elemendi (stringi) pikkust.
	// Meetodi nime v�id ise v�lja m�elda.

	public static int[] myNumbers(String[] numbrid) {
		int[] symbolCount = new int[numbrid.length];
		for (int i = 0; i < numbrid.length; i++) {
			symbolCount[i] = numbrid[i].length();
		}
		return symbolCount;

	}
	
	//�l 4
	public static byte centuryNumber (int yearNumber) {
		byte century;
		if (yearNumber > 0 || yearNumber < 2018) {
			century = (byte)(yearNumber / 100 + 1);
			return century;
		}
		return -1;
	}
}
