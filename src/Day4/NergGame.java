package Day4;

import java.util.Random;
import java.util.Scanner;

public class NergGame {

	public static void main(String[] args) {
		int randomNumber = new Random().nextInt(10000) + 1;
		System.out.println("paku number vahemikus 1- 10 000.");
		Scanner inputScanner = new Scanner(System.in); 
		//String decision = null;
		int counter = 1;
		int userGuess = 0;
		do {
			System.out.println("sinu pakkumine: ");
			userGuess = inputScanner.nextInt();
			if (userGuess == randomNumber) {
				System.out.println(String.format("�ige, sul kulus %s korda, et �ra arvata!",  counter));
			} else if (userGuess < randomNumber) {
				System.out.println("Liiga v�ike");
				counter++;
			} else {
				System.out.println("Liiga Suur");
				counter++;
			}

		} while (userGuess !=randomNumber);
		System.out.println("Copyright: Katrin Nevolainen. All rigts reserved");
	}
	
}
