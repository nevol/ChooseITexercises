package Day4;

public class LoopDemo {

	public static void main(String[] args) {
		
		int[] arr1 = {1, 2, 3, 4, 5 };
		//v�ljaspoolt ts�kli  i defineerimine ei ole OK!
		for (int i = 0; i < arr1.length; i++) { //i++ v�ib olla ka l�pus
			System.out.println( "Tere");
		}
		
		for (int i = arr1.length - 1; i >=0; i--) { // i = 4,3 2, 1, 0, -1 (finish) kuna -1 on <=0
			System.out.println( "Tere " + arr1[i]);
		}
		
		//for(;;); igavene ts�kkel
		//while(true); igavene
		
		//WHILE
		
		int u = 0;
		while (u < arr1.length) {
			System.out.println("while " + arr1[u++]);
		}
		
		int j = arr1.length - 1;
		while(j >= 0) {
			System.out.println("Moika " + arr1[j--]);
		}
			
		//FOREACH
		for (int x : arr1) {
			System.out.println("Moi " + x);
		}
		
		//DOWHIlE
		
		int k = 0;
		do {
			System.out.println("Hello " + arr1[k++]);
		} while (k < arr1.length);
		
		//BREAK
		
		for (int i = 1; i < 10000; i++) {
			if (i < 650) {
				System.out.println("L�petame selle ts�kli tegevuse");
				continue; //j�tkame uue ts�kliga
			}
			if (i % 650 ==0 ) {
				System.out.println(i);
				break; //l�petame ts�kli tegevuse
			}
		}
		
		//kole ja ebaviisakas meetod
		someLabel:
		for (int i = 0; i < 10; i++ ) {
			for (int s = 456; s < 634; s++) {
				if (s == 500) {
					break someLabel; 
				}
			}
		}
		
		//Viisakas
		someFuncWithTwoLoops();		
	}
	
	private static void someFuncWithTwoLoops() {
		for (int i = 0; i < 10; i++) {
			for (int s =456; s < 634; s++) {
				if (s ==500) {
					return;
				}
			}
		}
	}

	
}
