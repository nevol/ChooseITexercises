package Day4;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import com.sun.org.apache.xml.internal.utils.SystemIDResolver;

public class CollectionDemo {

	public static void main(String[] args) {

		int[] arr1 = { 5, 7, 2, 4, 6 };
		System.out.println(arr1[2]);
		arr1[2] = 98;
		System.out.println(arr1[2]);
		System.out.println(arr1.length);

		List<Integer> list1 = new ArrayList<>(Arrays.asList(5, 7, 2, 4, 6));
		list1.add(0, 65);
		list1.add(3, 100);
		list1.add(98);
		System.out.println(list1.get(3));
		System.out.println(list1);
		list1.remove(0);
		list1.removeIf(x -> x > 5);
		System.out.println(list1);
		System.out.println(list1.size());
		list1.addAll(Arrays.asList(5, 7, 2, 4, 6));
		System.out.println(list1);
		list1.forEach(x -> {
			System.out.println("Prindin v�lja elemendi...");
			System.out.println("    Elemendi v��rtus on " + x);
			System.out.println("    Elemendi v��rtuse kasv on " + (x + 1));
			System.out.println("--------");
		});

		for (Integer x : list1) {
			System.out.println("Prindin v�lja elemendi...");
			System.out.println("    Elemendi v��rtus on " + x);
			System.out.println("    Elemendi v��rtuse kasv on " + (x + 1));
			System.out.println("--------");
		}

		// List<Integer> list1 = new ArrayList<>();
		// list1.add(5);
		// list1.add(7);
		// list1.add(2);
		// list1.add(4);
		// list1.add(6);

		Set<String> set1 = new HashSet<>(Arrays.asList("viis", "kuus", "seitse"));
		// set1.add("seitse");
		// set1.add("kaheksa");
		System.out.println(set1);
		// String[] elementsFromSet = set1.toArray(new String[0]);
		// System.out.println(elementsFromSet[2]);
		set1.removeIf(x -> x.equals("viis") || x.equals("kuus"));
		// set1.remove("viis");
		// System.out.println(set1);

		Set<String> set2 = new TreeSet<>(Arrays.asList("viis", "kuus", "seitse"));
		System.out.println(set2);

		Set<String> set3 = new TreeSet<>(new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				return o2.compareTo(o1);
			}

		});
		set3.addAll(Arrays.asList("viis", "kuus", "seitse"));
		System.out.println(set3);

		Map<String, String> map1 = new HashMap<>();
		map1.put("auto", "car");
		map1.put("�ks", "one");
		map1.put("maja", "house");
		System.out.println(map1);
		System.out.println(map1.get("maja"));
		map1.remove("maja");
		System.out.println(map1);
		System.out.println(map1.keySet());
		System.out.println(map1.values());
		map1.forEach((a, b) -> {
			System.out.print(a + "key = ");
			System.out.println(b + "value");
		});
		
		for(String key : map1.keySet()) {
			System.out.println("KEY: " + key + " = " + map1.get(key));
		}

		Map<String, List<String>> map2 = new HashMap<>();
		map2.put("�ks", Arrays.asList("one", "eins", "uno"));
		System.out.println(map2);
		
		Map<String, Object> map3;
		
	}

}
