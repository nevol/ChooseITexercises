package Day4;

import java.util.Random;
import java.util.Scanner;

public class MarekGame {

	public static void main(String[] args) {
		System.out.println("Paku number vahemikus 1 - 10 000.");
		Scanner inputScanner = new Scanner(System.in);
		String decision = null;//deklareerin muutuja
		do {
			play(inputScanner);//meetodi k�ivitamine (v�ljakutusmine), meetod ise allpool //ALT + SHIFT + R = muudab meetorid/muutuja nime l�bi kogu koodi
			System.out.println("Kas soovid m�ngida uuesti? y/n");
			decision = inputScanner.next(); // v��rtustan muutuja
		} while(decision.equals("y"));
		
		System.out.println("Copyright: Marek Lints. All rights reserved!");
	}
//definitsioon meetod = funktsioon = subrutiin = protseduur = alamprogramm
	private static void play(Scanner inputScanner) {
		int randomNumber = new Random().nextInt(10000) + 1; //objektide loomise meetod
		int counter = 1;
		int userGuess = 0;
		do {
			System.out.println("Sinu pakkumine: ");
			userGuess = inputScanner.nextInt();
			if (userGuess == randomNumber) {
				System.out.println(String.format("�ige, sul kulus %s korda, et �ra arvata.", counter));
			} else if (userGuess < randomNumber) {
				System.out.println("Liiga v�ike.");
				counter++;
			} else {
				System.out.println("Liiga Suur.");
				counter++;
			}			
		} while (userGuess != randomNumber); //tingimus on t�ene siis, kui kasutaja arvas valesti. != on mittev�rdumise operaator
	}
	
}