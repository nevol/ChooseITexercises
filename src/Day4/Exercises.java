package Day4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Exercises {

	public static void main(String[] args) {
		
		for (long i = 0; i< 10_000_000L; i = i + 1) {
			System.out.println(i);
		}
		
		for(int i : new int[] {5, 6, 7, 8, 9}) {
			System.out.println(i);
		}
		
		boolean shouldContinue = true;
		while (shouldContinue) {
			
			if (Math.random() > 0.9) {
				System.out.println("L�petan ts�kli t��");
				//shouldContinue = false;
				break; 
			}
		} while (shouldContinue);
		
		//Kollekstioonid
		
		//nimekiri elementidest
		List<String> list1 = new ArrayList<>();
		
		//unikaalne nimekiri elementidest
		Set<String> set1 = new HashSet<>();
		
		//v�ti --> v��rtus paaride nimekiri
		Map<String, String> map1 = new HashMap<>();
		
		//Kollektasioon on massiiv (array)
		//Massiivil kindel pikkus, kollektsioonil pikkus v�ib muutuda
		//Kollekstioonil palju abimeetodeid, massivide� need puuduvad
		//Massiv ei oska enda sisu v�lja printida, kollektsioon oskab.
		//Massiiv r�iskab v�hem ressursse, kollektsioon on kallis (m�lu, protsessori j�udlus).
		
	}

}
