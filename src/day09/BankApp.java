package day09;

import java.io.IOException;
import java.util.Scanner;

public class BankApp {

	public static void main(String[] args) throws IOException {
		
		System.out.println("Bank system starts up");
		Account.importClients(args[0]);
		
		System.out.println("The bank has the following customers");
		System.out.println(Account.getClient());
		
		Scanner inputScanner = new Scanner(System.in);
		while(true) {
			System.out.println("Please insert command");
		//kuula sisendit
			String input = inputScanner.nextLine();
		//teosta operatsioon
			if (input.startsWith("TRANSFER")) {
				System.out.println("Starting money transfer");
				String[] inputParts = input.split(" ");
				Account.transfer(inputParts[1], inputParts[2], Integer.parseInt(inputParts[3])); //loob meetodi Account classi "public static void transfer"
			} else if (input.startsWith("BALANCE")) { //k�sklus algab "Balance"
				System.out.println("Displaying account balance ...");
				String[] inputParts = input.split(" ");
				if (inputParts.length == 2) {
					System.out.println("Searching by account number");
					Account.displayClientInfo(inputParts[1]);//create method (Account classi) [1] v�tame aluseks teise parameetri ehk public static void display DisplayClientInfo <String>
				} else {
					System.out.println("Searching by first name and last name");//loob meetodi Account classi "public static void transfer DisplayClientInfo <String> <String>
					Account.displayClientInfo(inputParts[1], inputParts [2]);
				}
				} else {
					System.out.println("Incorrect command");
				
					
			}
			
		}

	}
}


