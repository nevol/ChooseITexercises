package day09;

import java.util.ArrayList;
import java.util.List;

import day08.Car;
import day08.Ford;

public class App {

	public static void main(String[] args) {
		
		Car fordo1 = new Ford(); 
		fordo1.setColor("blue");
		fordo1.setMaxSpeed(180);
		fordo1.setSeatCount(5);
		
		Car fordo2 = new Ford(); 
		fordo2.setColor("blue");
		fordo2.setMaxSpeed(180);
		fordo2.setSeatCount(5);
		
		System.out.println("Ford 1 hashcode!: "+ fordo1.hashCode());
		System.out.println("Ford 2 hashcode!: "+ fordo2.hashCode());
		
		fordo1 = fordo2;
//		fordo2.setColor("red");
//		System.out.println(fordo1.getColor());
		
		
		
		System.out.println("V�rdleme kahte obektipointerit: " + (fordo1 == fordo2));
		
		System.out.println("Objekti v�rdlemine iseendaga: " + fordo1.equals(fordo1)); //objekt on alati v�rdne iseendaga
		System.out.println("Objekti v�rdlemine mittemillegagi: " + fordo1.equals(null));// objekt pole kunagi v�rdne mittemillegagai
		System.out.println("Auto onjekti v�rdlemine string objektiga: " + fordo1.equals("tere"));// auto Fordo1 ei ole v�rdne stringiga
		
		System.out.println("Objekti v�rdlemine teise samav��rse objektiga: " + fordo1.equals(fordo2));
		
		Car car1 = new Ford();
		car1.setColor("blue");
		car1.setMaxSpeed(160);
		car1.setSeatCount(5);
		
		Car car2 = new Ford();
		car2.setColor("green");
		car2.setMaxSpeed(160);
		car2.setSeatCount(5);
		
		Car car3 = new Ford();
		car3.setColor("red");
		car3.setMaxSpeed(130);
		car3.setSeatCount(7);
		
		Car car4 = new Ford();
		car4.setColor("black");
		car4.setMaxSpeed(80);
		car4.setSeatCount(8);
		
		System.out.println(car1.compareTo(car2));
		System.out.println(car2.compareTo(car1));
		System.out.println(car2.compareTo(car2));
		
		if (car1.compareTo(car2) < 1) {
			//esimene auto on "v�iksem" , kui teine
		}
		if (car1.compareTo(car2) > 1) {
			//esimene auto on "suurem" , kui teine
		if (car1.compareTo(car2) == 0) {
			//esimene auto on teisega v�rdne
		
		}
		
		List<Car> cars = new ArrayList<Car>();
		cars.add(car1);
		cars.add(car2);
		cars.add(car3);
		cars.add(car4);
		}
		}
	}


