package day02;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Exercises {
	
	public static void main(String[] args) {
		
		//mutuja, mis hoiaks v��rtust 456,78
		float f1 = 456.78f;
		double d1 = 456.78d;
		BigDecimal bd1 = new BigDecimal("456.78");
		
		//muutuja, mis viitaks t�htede jadale "test"
		String s1 = "test";
		
		//defineeri muutuja, mis hoiaks t�ev��rtust
		boolean b1 = true;
		
		//muutuja, mis viitab numbrite kogumile. 
		//Variant 1:
		//int[] numArray1 = null;
		int[] numArray1 = new int [4];
		numArray1[0] = 5;
		numArray1[1] = 91;
		numArray1[2] = 304;
		numArray1[3] = 405;
		
		for (int i = 0; i < numArray1.length; i++) {
			System.out.println(numArray1[i]);
		}
		
		//Variant 2
		int i1 = 6;
		int[] nummArray2 = { 5, 91, 304, 405, i1 };
		
		//muutuja mis viitab komakohaga mumbrite kogumile
		float[] f2 = {56.7f, 45.8f, 91.2f };
		
		//muutujad, mis hoiavad v��rtust 'a'. 
		String s2 = "a";
		char c1 = 'a'; 
		byte b2 = (byte) 'a';
		System.out.println((char)b2);
		
		//Erineva iseloomuga muutujate kogum
		String[] textArray = { "See on esimene v��rtus", "67", "58.92" };
		
		//suur number
		BigInteger bi1 = new BigInteger("76754545455454545454545454545454574544444444444444445545454555555555555454455");
		System.out.println(bi1.multiply(new BigInteger("2")));
		
		
	}

}
