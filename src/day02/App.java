package day02;

public class App {
	public static void main(String[] args) {
		
		int a = 1;
		int b = 1;
		int c = 3;
		System.out.println(a==b);
		System.out.println(a==c);
		
		a = c;
		System.out.println(a==b);
		System.out.println(a==c);
		
	    int x1 = 10;
		int x2 = 20;
		int y1 = ++x1;
		System.out.println(x1);
		System.out.println(x2);
		int y2 = x2++;
		System.out.println(y1);
		System.out.println(y2);
		
		String s1 = "text1";
		String s2 = new String ("text1");
		s1 = s1 + s2;
		
		System.out.println("Esimene parameeter: " + args [0]);
		System.out.println("Teine parameeter: " + args [1]);
		System.out.println(args [0] == args [1]);
		
		String t1 = args[0];
				
		int au = 345;
		
		String a1 = "Happy Birthday";
		String b1 = "Anita";
		System.out.println(a1 + " Dear " + b1 + " and welcome to the club!");
		
	}

}
