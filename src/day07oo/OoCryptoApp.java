package day07oo;

import java.io.IOException;

public class OoCryptoApp {

	public static void main(String[] args) throws IOException {

		// V�ta sisendtekst.
		// Loe sisse s�nastiks (failist).
		// Teosta konverteerimine.

		System.out.println("Algne tekst:");
		System.out.println(args[0]);

		Cryptor encryptor = new Encryptor("C:/tmp/20180417/alfabeet.txt");
		String encryptedText = encryptor.translate(args[0]);

		System.out.println("Kr�pteeritud tekst:");
		System.out.println(encryptedText);
		
		Cryptor decryptor = new Decryptor("C:/tmp/20180417/alfabeet.txt");
		System.out.println("Dekr�pteeritud tekst:");
		String decryptedText = decryptor.translate(encryptedText);
		System.out.println(decryptedText);

		
	}

}

