package day06;

import java.util.Scanner;

public class OoBasics {

	public static void main(String[] args) {
		//OoBasics ooBasics = new OoBasics() ;//objekt klassi endas //obejktil on omadused ja k�itumine(seisund)

		Dog myCoolDog = new Dog(); //uus objekt Dog on loodud klassi  Public class Dog baasil
		myCoolDog.setName("Muki");
		myCoolDog.setAge(6);
		//myCoolDog.setTailLength(40);
		//System.out.println(myCoolDog.legCount);
		System.out.println(myCoolDog.name);
		myCoolDog.bark(3);
		myCoolDog.run();
		
		Dog dog2 = new Dog();
		dog2.setName("Rex");
		dog2.setAge(3);
		System.out.println(dog2.name);
		dog2.bark(2);
		dog2.run();
		
		
		Dog dog3 = new Dog();
		dog3.setName("Jeff");
		dog3.setAge(3);
		
		Dog dog4 = new Dog();
		
		
		myCoolDog.printFellowCount(); //this.age 
		Dog.printDogCount(); //static
		Dog.printDogAge(myCoolDog); //static, mis n�uab  + this.age asemele  + DogWithAge.age
		Dog.printDogName(dog3); //static mis n�uab  + this.name asemele  + DogName.name
		dog2.printDogName(); //this.age
		dog2.printDogAge(); //this.age
	
//		Scanner scanner = new Scanner(System.in);
//		
//		Person person1 = new Person("38305230283");
//		System.out.println(person1.getBirthYear());
//		System.out.println(person1.getBirthMonth());
//		System.out.println(person1.getBirthDayOfMonth());
//		System.out.println(person1.getGender());
//		if (person1.getGender() == Gender.MALE) {
//			System.out.println("The person is male");
//		} else if (person1.getGender() == Gender.FEMALE) {
//				System.out.println("The person is female");	
//		} else {
//			System.out.println("The person is unknown");
		
//	}
	
	
}
}
