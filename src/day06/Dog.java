package day06;

public class Dog { 
 
//	protected int legCount = 4; //objekti koostisosad ehk virtuaalsed andmed, tekib reaalselt siis kui luuakse uus objekt antud klassist Dog myCoolDog = new Dog(ooBasics)
//	//private int tailLength = 0;
//	protected String name = null;
//	protected int age = 0;
//	public static int count = 0; // "Static" see muutuja ei kuulu objektile. klassi koostisosa, mis ei kuulu objektile
//	//Sellest count muutujast ei tehta objektile koopiat
	
	protected int legCount = 4;
	protected int age = 0;
	protected String name = null;
	


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getLegCount() {
		return this.legCount;
	}
	
	public int getAge() {
		System.out.println("The dog " + this.getName() + " age was requested...");
		return this.age;
	}
	
	public void setAge(int dogAge) {
		System.out.println("The dog " + this.getName() + " age was changed to " + dogAge);
		if (dogAge > 0 && dogAge < 25) {
			this.age = dogAge;
		} else {
			System.out.println("Invalid age setting attempt to " + this.name);
		}
	}
	
	private static int count = 0; // See muutuja ei kuulu objektile. 
	// Sellest count muutujast ei tehta objektile koopiat. 
	
	public static int getCount() {
		return count;
	}

	public Dog() {
		Dog.count++;
	}
	
	public void bark(int barkCount) {
		for(int i = 0; i < barkCount; i++) {
			System.out.println("Barking...");	
		}
	
	}
	public void run() {
		if (age > 5) {
			System.out.println("Run slowly");
		} else {
			System.out.println("Run fast");
		}
	}
	
	public void printFellowCount() {
		System.out.println("My name is " + this.name + " and I have " + Dog.count + " friends.");
	}
	
	public static void printDogCount() {
		System.out.println("You have created " + Dog.count + " dogs so far. Keep on going...");
	}
	
	public static void printDogAge(Dog DogWithAge) {
		System.out.println("The dog age is " + DogWithAge.age);
	}

	public static void printDogName(Dog DogName) { //minu tehtud  Dog.printDogName(dog2);
		System.out.println("My name is " + DogName.name);
		
	}

	public void printDogName() {
		System.out.println("My name is " + this.name + " and i have " + Dog.count + " friends");//dog2.printDogName();
		
	}

	public void printDogAge() {
			System.out.println("Dog age is " + this.age + " years"); //dog2.printDogAge();
		
	}
		
}


	
