package day06;

import java.util.ArrayList;
import java.util.List;

public class AthleteExampleExercise {

	public static void main(String[] args) {
		Runner myRunner1 = new Runner();
		myRunner1.setFirstname("Aita-Leida");
		myRunner1.setLastname("Kuusepuu");
		myRunner1.setAge(45);
		myRunner1.setGender("F");
		myRunner1.setWeight(89.4d);
		
		Runner myRunner2 = new Runner();
		myRunner2.setFirstname("Raul");
		myRunner2.setLastname("Rebane");
		myRunner2.setAge(34);
		myRunner2.setGender("M");
		myRunner2.setWeight(75.4d);
		
		Runner myRunner3 = new Runner();
		myRunner3.setFirstname("Lauri");
		myRunner3.setLastname("Lahe");
		myRunner3.setAge(27);
		myRunner3.setGender("M");
		myRunner3.setWeight(79.6d);
		
		
		Skydiver mySkydiver1 = new Skydiver(); //Skydiver myGenericAthlete = new myGenericAthlete();
		mySkydiver1.setFirstname("Katrin");
		mySkydiver1.setLastname("Kirsipuu");
		mySkydiver1.setAge(43);
		mySkydiver1.setGender("F");
		mySkydiver1.setWeight(67.2d);
		
		Skydiver mySkydiver2 = new Skydiver();
		mySkydiver2.setFirstname("Leo");
		mySkydiver2.setLastname("Laht");
		mySkydiver2.setAge(34);
		mySkydiver2.setGender("M");
		mySkydiver2.setWeight(74.3d);
		
		Skydiver mySkydiver3 = new Skydiver();
		mySkydiver3.setFirstname("Liisa");
		mySkydiver3.setLastname("Meri");
		mySkydiver3.setAge(22);
		mySkydiver3.setGender("f");
		mySkydiver3.setWeight(57.8d);
		
		List<Athlete> athletes = new ArrayList<>();
		athletes.add(myRunner1);
		athletes.add(myRunner2);
		athletes.add(myRunner3);
		athletes.add(mySkydiver1);
		athletes.add(mySkydiver2);
		athletes.add(mySkydiver3);
		
		
		System.out.println(athletes);
		
		athletes.sort(new AthleteComparator());
		
		System.out.println(athletes);
		
		
		
//List<Athlete> athletes = new ArrayList<>();
//		athletes.add(myRunner);
//		athletes.add(mySkydiver1);
		//athletes.add(myGenericAthlete);
//		
//		for(Athlete athlete : athletes) {
//			athlete.perform();
		}
		
	

}


