package day06;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class CountryInfo { //objekti defineerimine Exercise 2

	public String name; //seisundeid kirjeldavad muutujad
	public String capital;
	public String primeMinister;
	public List<String> languages = new ArrayList<>();
		
	public CountryInfo(String name, String Capital, String primeMinister, Collection<String> languages) {
		this.name = name;
		this.capital = capital;
		this.primeMinister = primeMinister;
		this.languages.addAll(languages);
		
	}
	
	@Override
	public String toString() {
	
		
		return null;
	}
	
}