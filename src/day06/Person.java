package day06;

public class Person { //objekti meetod
	
	private String personCode; //klassi baasil loome 
	
	public Person (String personCode) { //konstruktor meetodi abil loodud objekt
	 this.personCode = personCode; 
	}

	public int getBirthYear() {

			if (this.personCode != null && this.personCode.length() == 11) {
				int centuryDeterminant = Integer.parseInt(this.personCode.substring(0, 1));
				int birthYearInCentury = Integer.parseInt(this.personCode.substring(1,3));
				
				if (centuryDeterminant == 1 || centuryDeterminant == 2) {
					return 1800 + birthYearInCentury; 
				} else if (centuryDeterminant == 3 || centuryDeterminant == 4) {
						return 1900 + birthYearInCentury;
				} else if (centuryDeterminant == 5 || centuryDeterminant == 6) {
						return 2000 + birthYearInCentury;	
				}
			}
			return 0;	
	}
	public String getBirthMonth() {
		if(this.personCode != null && personCode.length() == 11) {
			String birthMonthNumber = personCode.substring(3, 5);
			switch(birthMonthNumber) {
			case "01":
				return "Jaanuar";
			case "02":
				return "Veebruar";
			case "03":
				return "M�rts";
			case "04":
				return "Aprill";
			case "05":
				return "Mai";
			case "06":
				return "Juni";
			case "07":
				return "Juuli";
			case "08":
				return "August";
			case "09":
				return "September";
			case "10":
				return "Oktoober";
			case "11":
				return "November";
			case "12":
				return "Detsember";
			}
		}
		return "teadmata";
	}
	
	public String getBirthDayOfMonth() {
		if(this.personCode != null && personCode.length() == 11) {
			return personCode.substring(5, 7);
		}
		return null;
		}
	
	public Gender getGender( ) {
		if (personCode != null) {
		int genderDeterminate = Integer.parseInt(personCode.substring(0, 1));
		if (genderDeterminate % 2 == 0) {
			return Gender.FEMALE;
		} else {
				return Gender.MALE;
		}
		} return Gender.UNKNOWN;
		}
	}

