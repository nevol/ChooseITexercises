package day06;

import java.util.Comparator;

public class AthleteComparator implements Comparator<Athlete> {

	@Override
	public int compare(Athlete o1, Athlete o2) {
		
		//sorting by Last name
		int result = o1.getLastname().compareTo(o2.getLastname());
		if (result == 0) {
			//sorting by first name
			result = o1.getFirstname().compareTo(o2.getFirstname());
			
			if (result == 0) {
				//sorting by age
				result = o1.getAge() - o2.getAge();
			
				
				//sorting by weight
				if(result == 0) {
					double weightDiff = o1.getWeight() - o1.getWeight();
					if (weightDiff > 0.0) {
						result = 1;
					} else if(weightDiff < 0.0) {
						result = -1;
					} else {
						result = 0;
					}
					//sorting by gender
					result = o1.getGender().compareTo(o2.getGender());
				}
		}
					
}
		return result;
		
		
		
		
	
		
//		if (o1.getAge( < 02.getAge()); {
//			return -1;
//		} else if (01.get.Age() > 02.getAge()) {
//			return 1;
//		}
//		
//		return 0;
		
		//kui 01 on v�iksem, kui02 --> tagasta negatiivne number
		//kui 01 on suurem kui 02, tagasta pos number
		//kui 01 ja 02 on v�rdsed, tagatsa 0
	}

}
