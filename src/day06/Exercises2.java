	package day06;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exercises2 {

	public static void main(String[] args) {

		CountryInfo estonia = new CountryInfo("Estonian", "Tallinn", "Juri Ratas",
			Arrays.asList("Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"));
			
		CountryInfo latvia = new CountryInfo("Latvia", "Riga", "Maris Kucinskis",
			Arrays.asList("Estonian", "Russian", "Ukrainian", "Belarusian", "Finnish"));
			
		System.out.println(estonia);
		
//		estonia.name = "Estonia";
//		estonia.capital = "Tallinn";
//		estonia.primeMinister = "Juri Ratas"; 
//		estonia.languages.addAll(Arrays.asList("Estonian", "Russian", "Ukrainian", "Belarusioan", "Finnish"));
		
//		System.out.println(estonia.name);
//		for(String language : )
		
//		CountryInfo latvia = new CountryInfo();
//		estonia.name = "Latvia";
//		estonia.capital = "Riga";
//		estonia.primeMinister = "Maris Kuncinskis";
//		latvia.languages.add("Latvian");
//		latvia.languages.add("Russian");
//		latvia.languages.add("Belarusian");
//		latvia.languages.add("ukrainian");
//		latvia.languages.add("Polish");
		
		List<CountryInfo> countries = new ArrayList<>();
		countries.add(estonia);
		countries.add(latvia);
		
		for(CountryInfo country : countries) {
			System.out.println(country);
		}
		
	
		
	}
}
