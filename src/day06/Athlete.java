package day06;

public abstract class Athlete { //objekti klass
   //objekti muutujad
	private String firstname;
	private String lastname;
	private int age;
	private String gender;
	private double weight;
	private int length;
	
	
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}


	


	
	public abstract void perform();
	
	protected void prepareForPerform() {
		//valmistame sportlase ette sportimiseks
	}
	
	@Override
	public String toString() {
		return String.format("\n[fn=%s, ln=%s, a=%s, g=%s, w=%s]", this.getFirstname(), this.getLastname(), this.getAge(), this.getGender(), this.getWeight());
	}
	
	}