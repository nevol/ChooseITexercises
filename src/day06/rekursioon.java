package day06;

public class rekursioon {

	public static void main(String[] args) {
		
		//rekursiivne meetod
		
		for (int i = 100; i > 0; i--); {
			System.out.println(i);
		}
		
		printOutNumbers(100);
	}

	static void printOutNumbers(int i) {
		System.out.println(i);
		i = i - 1;
		if (i > 0) {
			printOutNumbers(i);
			
		}
	}
}
