package day08;

//import day06.Dog;
//
//public class SmallEnglishDog extends Dog {
//	
//	@Override
//	public void bark(int barkCount) {
//			System.out.println("yap-yap");
//		}
//	}

import day06.Dog;

public class SmallEnglishDog extends Dog {
	
	@Override
	public void bark(int barkCount) {
		System.out.println("yap-yap");
	}
	
	public void englishStyleBarking() {
		System.out.println("yap-yap");
	}
	
}
