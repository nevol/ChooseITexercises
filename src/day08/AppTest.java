package day08;

//import static org.junit.Assert.*;
//
//import java.math.BigInteger;
//
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.BeforeClass;
//import org.junit.Test;
//
//public class AppTest {
//
//	@BeforeClass
//	public static void setUpBeforeClass() throws Exception { //käivitatakse igal juhul
//	}
//
//	@AfterClass
//	public static void tearDownAfterClass() throws Exception { //käivitatakse igal juhul
//	}
//
//	@Before
//	public void setUp() throws Exception { //alati enne test meetodi käivitamist
//	} 
//
//	@After
//	public void tearDown() throws Exception { //alati enne test meetodi käivitamist
//	}
//
////	@Test
////	public void testAddNumbers() {
////		int result = App.addNumbers(2, 2);
////		assertTrue(result == 4);
////		assertEquals(4, result);
//	//}
//
//	
//	//day06 class exercise1, ül 7
//	@Test
//	public void testIsCheckNumberCorrect() {
//		boolean result = day06.Exercises1.isCheckNumberCorrect(new BigInteger("38104242729"));
//		assertTrue(result);
//		
//		result = day06.Exercises1.isCheckNumberCorrect(null);
//		assertFalse(result);
//		
//		result = day06.Exercises1.isCheckNumberCorrect(new BigInteger("1345"));
//		assertFalse(result);
//		
//		result = day06.Exercises1.isCheckNumberCorrect(new BigInteger("38104242728"));
//		assertFalse(result);
//		
//	} 
//	
//	@Test
//	public void testIsPersonalCodeCorrect() {
//		boolean result = day06.Exercises1.isPersonalCodeCorrect("38104242729");
//		assertTrue(result);
//		
//		result = day06.Exercises1.isPersonalCodeCorrect("38145242729");
//		assertFalse(result);
//		
//		result = day06.Exercises1.isPersonalCodeCorrect("38104782729");
//		assertFalse(result);
//		
//		result = day06.Exercises1.isPersonalCodeCorrect("7810230272");
//		assertFalse(result);
//	}
//
//	@Test
//	public void testDeriveGender() {
//		assertTrue(day06.Exercises1.deriveGender("381").equals("M"));
//		assertTrue(day06.Exercises1.deriveGender("481").equals("N"));
//		
//		
//		
//	}
//} 

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class AppTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddNumbers() {
		int result = App.addNumbers(2, 2);
		assertTrue(result == 4);
		assertEquals(4, result);
	}
	
	@Test
	public void testIsCheckNumberCorrect() {
		boolean result = day06.Exercises1.isCheckNumberCorrect(new BigInteger("38104242729"));
		assertTrue(result);
		
		result = day06.Exercises1.isCheckNumberCorrect(null);
		assertFalse(result);
		
		result = day06.Exercises1.isCheckNumberCorrect(new BigInteger("1345"));
		assertFalse(result);
		
		result = day06.Exercises1.isCheckNumberCorrect(new BigInteger("38104242728"));
		assertFalse(result);
	}
	
	@Test
	public void testIsPersonalCodeCorrect() {
		boolean result = day06.Exercises1.isPersonalCodeCorrect("38104242729");
		assertTrue(result);
		
		result = day06.Exercises1.isPersonalCodeCorrect("38145242728");
		assertFalse(result);
		
		result = day06.Exercises1.isPersonalCodeCorrect("38104782721");
		assertFalse(result);
		
		result = day06.Exercises1.isPersonalCodeCorrect("78104242722");
		assertFalse(result);
	}
	
	@Test
	public void testDeriveGender() {
		assertTrue(day06.Exercises1.deriveGender("381").equals("M"));
		assertTrue(day06.Exercises1.deriveGender("481").equals("F"));
	}

}