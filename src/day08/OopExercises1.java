package day08;

//import day06.Dog;
//
//public class OopExercises1 {
//
//	public static void main(String[] args) {
//		
//		//�l 1
//		Dog smallEnglishDog = new SmallEnglishDog();
//		Dog polishDog = new PolishDog();
//		
//		System.out.println("Small English dog: ");
//		smallEnglishDog.bark(1);
//		
//		System.out.println("Polish dog: ");
//		polishDog.bark(3);
//
//
//	
//	Dog[] barkingDogs = new Dog[2];
//	barkingDogs[0] = smallEnglishDog;
//	barkingDogs[1] = polishDog;
//	
//	makeDogsBark(barkingDogs);
//	
//	}
//	public static void makeDogsBark(Dog[] dogs) {
//		for(Dog barkingDog : dogs) {
//			barkingDog.bark(5);
//		}
//	}
//}

import day06.Dog;

public class OopExercises1 {

	public static void main(String[] args) {
		
		SmallEnglishDog smallEnglishDog = new SmallEnglishDog();
		Dog polishDog = new PolishDog();
		
		smallEnglishDog.englishStyleBarking();
		
		System.out.print("Small English dog: ");
		smallEnglishDog.bark(5);
		
		System.out.print("Polish dog: ");
		polishDog.bark(5);
		
		Dog[] barkingDogs = new Dog[2];
		barkingDogs[0] = smallEnglishDog;
		barkingDogs[1] = polishDog;
		
		makeDogsBark(barkingDogs);
		
	}
	
	public static void makeDogsBark(Dog[] dogs) {
		for(Dog barkingDog : dogs) {
			barkingDog.bark(5);
		}
	}

}



