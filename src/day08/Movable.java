package day08;

public interface Movable { //loome, et objektidel Ford, Renault ja Ferrari oleks tegevus "drive"

	void setColor(String color);
	String getColor();
	
	void setMaxSpeed(int speed);
	int getMaxSpeed();
	
	void setSeatCount(int seatCount);
	int getSeatCount();
	
	void drive();
}