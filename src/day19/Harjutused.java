package day19;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Harjutused {

	private static final List<String> String = null;

	public static void main(String[] args) {

		// �L1
		int[] ruuduMassiv = { 5, 8, 9 };
		for (int num : massiivRuudus(ruuduMassiv))
			System.out.println(num);
		// �l2
		List<String> oddityStatus = createList(new int[] { 5, 8, 9, 10, 11 });
		System.out.println(oddityStatus);
		// �l3
		List<Integer> list = createNumberList(5);
		System.out.println(list);
		// �l4
		List<String> inputList = new ArrayList<>();
		inputList.add("173");
		inputList.add("45");
		inputList.add("23");
		inputList.add("1");
		List<Integer> convertList = convertNumbers(inputList);
		System.out.println(convertList);
		// �l5
		// Map<String, String> annaTulemust = annaIsikuAndmed();
		// System.out.println(annaTulemust.get("Elukoht"));
		System.out.println(annaIsikuAndmed().get("Vanus"));
		
		// �l 4 - v�ljakutsumine        
		List<Integer> inputList1 = new ArrayList<>();        
		inputList1.add(67);        
		inputList1.add(343);        
		inputList1.add(45);        
		List<Integer> inputList2 = new ArrayList<>();        
		inputList2.add(1);       
		inputList2.add(323);        
		inputList2.add(998);        
		List<Integer> myResultList= sumUpLists(inputList1, inputList2);        
		for (int sum : myResultList) {            
			System.out.println(sum);
		}

	}

	// �l1
	// Kirjutada Java funktsioon, mis v�tab sisendparam t�isarvude massivi ja
	// tagastab t�isarvude massiivi
	// tagastatava massiivi iga element on vastava sisendmassivi ruut
	// 2.7.9.--> /4, 49, 81}

	public static int[] massiivRuudus(int[] numbriteMassiv) {
		int[] vastus = new int[numbriteMassiv.length];
		for (int i = 0; i < numbriteMassiv.length; i++) {
			vastus[i] = numbriteMassiv[i] * numbriteMassiv[i];

		}
		return vastus;

	}

	// �L2 Funktsioon, mis v�tab sisendparameetriks t�isarvude massivi.
	// Funktsioon tagastab tekstide listi, kus iga listielement omab v��rtust kas
	// "paaris" v�i"paaritu"
	// sisend: {5, 7, 3, 2 } --> list ("paaritu", "paaritu", "paaritu", "paaris")

	public static List<String> createList(int[] numbers) {
		List<String> myResult = new ArrayList<>();

		for (int i = 0; i < numbers.length; i++) {
			if (numbers[i] % 2 == 0) {
				myResult.add("paaris");
			} else {
				myResult.add("paaritu");
			}
		}

		return myResult;

	}
	// �l 3
	// funktsioon, mis v�tab sisendparameetrina t�isarvu
	// v�ljund t�isarvude list
	// n�ide: sisend 4 --> list(0, 1, 2, 3)
	// n�ide: sisend 3 --> list(0, 1, 2)

	public static List<Integer> createNumberList(int number) {
		List<Integer> result = new ArrayList<>();
		for (int i = 0; i < number; i++) {
			result.add(i);
		}
		return result;

	}

	// �L4
	// sisend: tekstide list
	// v�ljund: t�isarvude list
	// funktsioon konverteerib iga sisendlisti elemendi t�isarvuks ja paneb selle
	// v�ljundisse
	// LIST: ("1", "323") -->LIST (1, 323, 998)

	public static List<Integer> convertNumbers(List<String> input) {
		List<Integer> output = new ArrayList<>();
		// for(String number : text) {
		// convertResult.add(Integer.parseInt(number));
		for (int u = 0; u < input.size(); u++) { // 2 variant
			output.add(Integer.parseInt(input.get(u)));
		}
		return output;

	}

	// �L5
	// funktsioon, mis ei v�ta sisndparameetreid
	// mis v�ljastab MAP elemendi isikuandmetega
	// Nimi = Katrin
	// Vanus --> 37
	// Elukoht --> Eesti
	// printige elukoht v�lja main meetodis

	public static Map<String, String> annaIsikuAndmed() {
		Map<String, String> personalDataMap = new HashMap<>();
		personalDataMap.put("nimi", "Katrin");
		personalDataMap.put("Elukoht", "Saue");
		personalDataMap.put("Vanus", "34");

		return personalDataMap;

	}

	// �lesanne 4
	public static List<Integer> sumUpLists(List<Integer> list1, List<Integer> list2) {
		List<Integer> result1 = new ArrayList<>();
			for (int i = 0; i < list1.size(); i++) {
			result1.add(list1.get(i) + list2.get(i));
		}
		return result1;
	}
}
