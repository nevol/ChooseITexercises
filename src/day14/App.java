package day14;

public class App {

	public static void main(String[] args) {
		System.out.println(getNumericSumOfChars("a"));
		System.out.println(getNumericSumOfChars("aa"));
		System.out.println(getNumericSumOfChars("aaa"));
		System.out.println(getNumericSumOfChars("b"));
		System.out.println(getNumericSumOfChars("ab"));
		
		//�L 2 (algus CoffeMachine classis)
		//�lesanne 2:Kirjutada klass CoffeMachine, millel on j�rgmised omadused:- beanCount (ubade arv)- waterAmount (vee hulk milliliitrites)
		//Klass defineerib ka meetodi makeCoffee(), mis ei v�ta �htki sisendparameetrit 
		//ja mis tagastab t�ev��rtuse vastavalt sellele, kas kohvi valmistamine �nnestus v�i mitte.
		//Iga kohv, mida masin valmistab, kulutab 100 uba ja 300 milliliitrit vett.Kui vett-ube piisavalt, 
		//tagastab makeCoffe meetod true, vastasel juhul false.Loo antud klassist objekt ja vamista m�ned tassid kohvi.
		
		CoffeeMachine myCoffeeMachine = new CoffeeMachine(); 
		myCoffeeMachine.setBeanCount(1500); //meetodi v�ljakutsuminevee ja ubade koguse sisestamine k�sitsi
		myCoffeeMachine.setWaterAmount(1700);
		System.out.println("Valmistame kohvi. Kas �nnestus? " + myCoffeeMachine.makeCoffee());
		System.out.println("Valmistame kohvi. Kas �nnestus? " + myCoffeeMachine.makeCoffee());
		System.out.println("Valmistame kohvi. Kas �nnestus? " + myCoffeeMachine.makeCoffee());
		System.out.println("Valmistame kohvi. Kas �nnestus? " + myCoffeeMachine.makeCoffee());
		System.out.println("Valmistame kohvi. Kas �nnestus? " + myCoffeeMachine.makeCoffee());
		System.out.println("Valmistame kohvi. Kas �nnestus? " + myCoffeeMachine.makeCoffee());
		System.out.println("Valmistame kohvi. Kas �nnestus? " + myCoffeeMachine.makeCoffee());
	}
	
	//�l 1 Kirjutada funktsioon, mis arvutab sisendparameetrina 
	//etteantud stringi k�igi t�htede numbriliste v��rtuste summa ja tagastab selle t�isarvuna.
	
	private static int getNumericSumOfChars(String text) {
		int result = 0;
		
		char[] textChars = text.toCharArray();
		for (char c : textChars) {
			result += (int) c;
		}
		return result;
	}

}
