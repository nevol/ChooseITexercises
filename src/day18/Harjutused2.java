package day18;

import com.sun.org.apache.bcel.internal.generic.RETURN;

public class Harjutused2 {

	public static void main(String[] args) {

		String[] result1 = multiplyText("Tere", 2); // multiply v�ljakutsumine

		for (String text : result1) {
			System.out.println(text);
		}
		String[] result2 = multiplyText("Hello", 2);
		for (String text : result2) {
			System.out.println(text);
		}

		String[] minuMassiiv = { "neli", "kaks", "kolm","seitse", "viisk�mmend" };
		for (int tahtedeArv : textSymboliteArv(minuMassiiv)) {
			System.out.println(tahtedeArv);
		}

		int[] generateArray = generateArray(5);
		for (int resultingArray : generateArray) {
			System.out.println(resultingArray);
		}
		

		int[] someNumbers = { 5, 8, 9, 2, 6, 13, 15, 14, 18 };
		boolean isFound = isInArray(someNumbers, 7);
		System.out.println(isFound);

		int[] numbrid = { 5, 8, 9, 2, 6, 13, 15, 14, 18 }; // �L5
		for (int num : multiplyArray(numbrid))
			System.out.println(num);

		// �l 6
		String[] s�nad = { "Auto", "Puu", "Rakett", "Hiir" };
		System.out.println(isTextInArray(s�nad, "Ferrari"));

		// �L7
		int[] jagamiseMasiiv = { 5, 8, 9, 2, 6, 13, 15, 14, 18 }; 
		for (int rida : numberDevidedByTwo(jagamiseMasiiv))
		System.out.print(rida);

		// String[] minuMassiiv1 = new String[] { "Auto", "Puu", "Rakett"}; //var 1
		//
		// String[] minuMassiv2 = new String[3]; //var 2. massivi pikkust ei saa muuta,
		// defineeritud 3
		// minuMassiv2[0] = "Auto";
		// minuMassiv2[1] = "Puu";
		// minuMassiv2[2] = "Rakett";
		//
		//
		// String[] minuMassiiv3 = { "Auto", "Puu", "Rakett", "Hiir" }; // var 2, sama.
		// minuMassiiv3[2] = "Laev"; //omistamine, muudame massivi 2 elemendi Laevaks
		// System.out.println(minuMassiiv3[2]);

	}

	// int[] someNumbers5 = { 5, 7, 8, 9, 3, 5, 6, 998, 345, 456,345 };
	// System.out.println(someNumbers5[3]);
	// someNumbers5[3] = 123456; // omistamine
	// System.out.println(someNumbers5[3]); // p�rimine

	public static String[] multiplyText(String text, int massiiviPikkus) {
		String[] result = new String[massiiviPikkus];
		for (int i = 0; i < massiiviPikkus; i++) {
			result[i] = text; // v��rtustame massiivi 'i' elementi
		}

		return result;

	}

	public static int[] textSymboliteArv(String[] text) {
		int[] tulemus = new int[text.length];
		for (int i = 0; i < text.length; i++) {
			tulemus[i] = text[i].length();
		}

		return tulemus;
	}

	public static int[] generateArray(int number) {
		int[] resultingArray = new int[number];
		for (int arrayIndex = 0; arrayIndex < resultingArray.length; arrayIndex++) {
			resultingArray[arrayIndex] = arrayIndex;
		}
		return resultingArray;

	}

	private static boolean isInArray(int[] someNumbers, int testNumber) {
		for (int index = 0; index < someNumbers.length; index++) {
			int xxx = someNumbers[index]; // massiivi elemendi poole p��rdumine
			if (xxx == testNumber) {
				return true;
			}
		}
		return false;

	}

	// �L 5 v�tab sisse t�isarvude massiivi, tagastab t�isarvude massivi
	// korrutab ise sisendmassivi elemendi kahega

	private static int[] multiplyArray(int[] numbrid) {
		int[] tulemus3 = new int[numbrid.length];
		for (int i = 0; i < numbrid.length; i++) {
			tulemus3[i] = 2 * numbrid[i];
		}
		return tulemus3;
	}

	// �L 6 Kirjutada Java funktsioon, mis v�tab sisendiks stringide masssiivi ja
	// kontrolllteksti ja tagastab
	// t�ev��rtuse vastavalt sellele, kas m�ni massiivi element v�rdub
	// kontrolltekstiga.
	// funtksiooni nimi isTextInArray (String[] stringideMassiiv, String
	// kontrollTekst}

	public static boolean isTextInArray(String[] stringideMassiiv, String kontrollTekst) {
		for (int i = 0; i < stringideMassiiv.length; i++) {
			if (kontrollTekst.equalsIgnoreCase(stringideMassiiv[i])) {
				return true;
			}
		}
		return false;
	}
	// �l7
	// function, mis v�tab sisendiks t�isarvude massivi ja tagastab t�sarvude massivi.
	// tagastatava massiivi iga element v�rdub sisendmassiivi vastava elemendiga, kui see
	// element ei ole kahega jauguv.
	// Kui on jaguv, pannakse tagastatava massivi vastava elemendi v��rtuseks 0;

	public static int[] numberDevidedByTwo(int[] numbriteMassiiv) {
		int[] tulemus = new int[numbriteMassiiv.length];
		for (int i = 0; i < numbriteMassiiv.length; i++) {
			if(numbriteMassiiv[i] % 2 == 0) {
				tulemus[i] = 0;
			} else {
				tulemus[i] = numbriteMassiiv[i];
			}
		}
		return tulemus;
	}

}
