package day18;

import com.sun.javafx.css.CalculatedValue;

public class Harjutused {

	public static void main(String[] args) {

		System.out.println(calculateBodyMassIndex(67.0f, 168));
		System.out.println(getBodyMassIndexCategory(23.74f));

	}

	public static float calculateBodyMassIndex(float kaalKg, int pikkusCm) {
		float pikkusM = pikkusCm / 100.0f;
		return Math.round(kaalKg / (Math.pow(pikkusM, 2)) * 100.0f) / 100.0f;

	}
	
	public static String getBodyMassIndexCategory (float bodyMassIndex) {
		if (bodyMassIndex >16) {
			return "Tervisele ohtlik alakaal";
		} else if (bodyMassIndex > 19) {
			return "alakaal";
		} else if (bodyMassIndex > 25) {
			return "Tervisele ohtlik alakaal";
		} else if (bodyMassIndex < 30) {
			return "�lekaal";
		} else if (bodyMassIndex < 35) {
			return "rasvumine";
		} else if (bodyMassIndex < 40) {
			return "Tugev rasvumine";
		}
		return "Tevisele ohtlik rasvumine";
		
	}

}
