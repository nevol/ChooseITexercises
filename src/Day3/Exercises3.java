package Day3;

import java.util.Scanner;

public class Exercises3 {
	
	private final static double VAT_PERCENTAGE = 0.2d;

	public static void main(String[] args) {
		Exercises3 myApp = new Exercises3();//uus muutuja mitteprimitiivne
		
	StringBuilder sb = new StringBuilder();
	String president1 = "Konstantin P�ts";
	String president2 = "Lennart Meri";
	String president3 = "Arnold R��tel";
	String president4 = "Toomas Hendrik Ilves";
	String president5 = "Kersti Kaljulaid";
	
	//String text + president1 + " , " + president2 v�ga mahukas
	
	sb.append(president1)
	.append(", ")
	.append(president2)
	.append(", ")
	.append(president3)
	.append(", ")
	.append(president4)
	.append(" ja ")
	.append(president5)
	.append(" on Eesti presidendid. ");
	System.out.println(sb.toString());
	
	String[] stringArray1 = {"text1", "text2", "text3"};
	
//String resultString = "";
	for(String item : stringArray1) { //ajutine muutuja
//resultStr1 = resultStr1 + item;
		sb.append(item);
	}
	
	System.out.println(sb.toString());
	
	printSentence("Rehepapp");
	printSentence("Kokakunst");
	printSentence("100 �hus�idukit");
	
	String bookTitle = "Rehepapp";
	String text1 = "Raamatu pealkiri on \"" + bookTitle + "\".";
	System.out.println(text1);
	
	//klassikaline objekti loomine
	
	String someText = "test";
	String someText2 = new String("test");
	
	int i = 7;
	Integer i2 = 7; //sama mis new Integer(7);
	//byte b = i2 byteValue();
	System.out.println(Integer.toBinaryString(7));
	int i3 = i + i2;
	
	//ParseInt ja toString 
	
	System.out.println(args[0]);
	System.out.println(args[1]);
	
	int in1 = Integer.parseInt(args[0]);
	float in2 = Float.parseFloat(args[1]);
	float sum = in1 + in2;
	System.out.println(sum);
	
	String sumStr = ((Float)sum).toString(); //luuakse objekt
	String sumStr2 = String.valueOf(sum); //sama l�henemine, luuakse string
	System.out.println(sumStr);
	
	//String split meetodi n�ide
	String str3 = "teks1, tekst2, tekst3, tekst4, tekst5";
	String[] str3Arr = str3.split(", ");
	for(String strItem : str3Arr) {
		System.out.println(strItem);
	}
	
	//Final �lesanne n�ide
	double price = 100.0d;
	price = price + price * VAT_PERCENTAGE;
	System.out.println(price);
	
	doExercises2();
			
	}
//Kutsutakse v�lja klassi sees printSentenc-iga (see on varjatu meetod)
	private static void printSentence (String bookTitle) {
		String text1 = "Raamatu pealkiri on \"" + bookTitle	+ "\".";
		System.out.println(text1);
	}
	
	//meetorid nimes v�ik olla k�sk - do jne
	private static void doExercises2() { 
		String s1 = "Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida.";
		Scanner sc = new Scanner(s1);
		sc.useDelimiter("Rida: ");
		while (sc.hasNext()) {
			System.out.println(sc.next());
		}
				
	}
}
