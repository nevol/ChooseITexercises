package Day3;

public class Exercises {

	public static void main(String[] args) {
		String city = args[0];
		doExercise2(city);
		
		int mark = new Integer(args[0]);
		//int mark = Integer.parseInt(args[0]);
		doExercises3(mark);
	
		
		int age = Integer.parseInt(args[0]);
		doExercise5(age);
		
		
		
	}
		

	private static void doExercise2(String city) {
		if ("Milano".equalsIgnoreCase(city)) {
			System.out.println("Ilm on soe");
		} else {
			System.out.println("Ilm polegi k�ige t�htsam");
		}
	}
	static void doExercises3(int mark) {
		if (mark == 1) {
			System.out.println("N�rk");
		} else if (mark == 2) {
			System.out.println("Mitterahuldav");
		} else if (mark == 3) {
			System.out.println("Rahuldav");
		} else if (mark == 4) {
			System.out.println("Hea");
		} else if (mark == 5) {
			System.out.println("V�ga hea");
		}
		
	}
	
	static void doExercise4(int mark) {
		switch(mark) {
		case 1:
			System.out.println("N�rk");
			return;
		case 2:
			System.out.println("Mitterahuldav");
		case 3:
			System.out.println("Rahuldav");
			break;
		case 4:
			System.out.println("Hea");
			break;
		case 5:
			System.out.println("V�ga hea");
			return;
		}
	}
	
	public static void doExercise5(int age) {
		System.out.println(age > 100 ? "Vana" : (age==100) ? "Peaaegu vana" : "Noor");
	}
}
