package Day3;

public class Exercises03 {

	public static void main(String[] args) {
		int[] numbers = new int[5];
		numbers[0] = 1;
		numbers[1] = 2;
		numbers[2] = 3;
		numbers[3] = 4;
		numbers[4] = 5;
		System.out.println(numbers[0]);
		System.out.println(numbers[2]);
		System.out.println(numbers[numbers.length - 1]);	

	

	String[] cities = { "Tallinn", "Helsinki", "Madrid", "Paris"};
	
	int[][] numbers2 = { {1, 2, 3}, {4, 5, 6}, {7, 8, 9, 0} };
	
	int[][] numbers3 = new int[3][];
	numbers3[0] = new int[] {1, 2, 3 };
	numbers3[1] = new int[] {4, 5, 6 };
	numbers3[2] = new int[] {7, 8, 9, 0 };
	
	int[][] numbers4 = new int[3][3];
	numbers4[0][0] = 1;
	numbers4[0][1]= 2;
	numbers4[0][2]= 3;
	numbers4[1][0] = 4;
	numbers4[1][1]= 5;
	numbers4[1][2]= 6;
	numbers4[2] = new int[] {7, 8, 9, 0 }; 
	
	for(int[] nums : numbers4) {
		for (int num : nums) {
			System.out.print(num + " ");
		}
		System.out.println();

	}
	
	String[][] countryCities = new String[3][4];
	countryCities[0][0] = "Tallinn";
	countryCities[0][1] = "Tartu";
	countryCities[0][2] = "V�ru";
	countryCities[0][3] = "Valga";
	countryCities[1][0] = "Stockholm";
	countryCities[1][1] = "Uppsala";
	countryCities[1][2] = "Lund";
	countryCities[1][3] = "K�ping";
	countryCities[2][0] = "Helsinki";
	countryCities[2][1] = "Espoo";
	countryCities[2][2] = "Hanko";
	countryCities[2][3] = "J�ms�";

	String[][] countryCities2 = { 
			{ "Tallinn", "Tartu", "V�ru", "Valga" },
			{ "Stockholm", "Uppsala", "Lund", "K�ping" }, 
			{ "Helsinki", "Espoo", "Hanko", "J�ms�" } 
		};
	
	String[][] countryCities3 = new String[3][];
	countryCities3[0] = new String[] {"Tallinn", "Tartu", "V�ru", "Valga"};
	countryCities3[1] = new String[] {"Stockholm", "Uppsala", "Lund", "K�ping"};
	countryCities3[2] = new String[] {"Helsinki", "Espoo", "Hanko", "J�ms�"};
	
	for(String[] tmpCities : countryCities3) {
		for (String tmpCity : tmpCities) {
			System.out.print(tmpCity + " ");
		}
		System.out.println();
}
	}
}


