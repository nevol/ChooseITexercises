package Day3;

public class MassivDemo {

	public static void main(String[] args) {
		
		String [][] users = { //stringide massiiv
				{ "Marek", "Lints", "Tallinn", "Eesti" }, //massiivid ise
				{ "Leida", "Kuusk", "Tartu" }, 
				{ "Tanel", "Tuisk", "Elva" } , 
				//user
			};
		
		String[][]users2 = new String[3][3];
		users2[0][0] = "Marek";
		users2[0][1] = "Lints";
		users2[0][2] = "Tallinn";
		users2[1][0] = "Leida";
		users2[1][1] = "Kuusk";
		users2[1][2] = "Tartu";
		users2[2] = new String[] {"Tanel", "Tuisk", "Elva"};
//		users2[2][0] = "Tanel";
//		users2[2][1] = "Tuisk";
//		users2[2][2] = "Elva";
		
		//ts�kkel
		for(String[] u : users2) {
			for (String userData : u) {
				System.out.print(userData + " ");
			}
			System.out.println();
			
		
		}

	}

}
