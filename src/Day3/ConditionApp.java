package Day3;

public class ConditionApp {

public static void main(String[] args) {
		
		int number = Integer.parseInt(args[0]);
		boolean oddityCheck = number % 2 == 0;
		
		if (!oddityCheck) {
			System.out.println("Etteantud arv " + number + " on paaritu.");
		} else {
			System.out.println("Etteantud arv " + number +" on paaris.");
		}

		String inlineIfResult = (number % 2 == 0) ? 
				"Etteantud arv " + number +" on paaris." 
			: 
				"Etteantud arv " + number + " on paaritu.";
		
		int inlineIfResult2 = (number % 2 == 0) ? 456 : 789;
		
		System.out.println(inlineIfResult);
		
	}

}
